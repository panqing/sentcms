<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\model\module;

use think\facade\Db;

/**
 * 文档模型
 */
class Document {

	public static function getDocumentList($model, $category_id, $limit = 20, $order = "id desc", $field = "*"){
		$map = [];
		if (!$model) {
			return [];
		}
		//判断model是否为内容模型
		$info = Model::where('name', $model)->where('status', 1)->findOrEmpty();

		if ($info->isEmpty()) {
			return [];
		}
		$list = Db::name($model)->where($map)->limit($limit)->order($order)->field($field)->select();

		return $list;	
	}
}