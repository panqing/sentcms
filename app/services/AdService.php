<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\services;

use app\model\ads\Ad;
use app\model\ads\AdPlace;

class AdService{
	
	/**
	 * @title 获取广告位列表
	 *
	 * @return void
	 */
	public function getAdPlaceList($request){
		$map = [];
		$order = "id desc";

		$list = AdPlace::where($map)->order($order)->paginate($request->pageConfig);

		return $list;
	}

	/**
	 * @title 获取广告位信息
	 *
	 * @param [type] $request
	 * @return void
	 */
	public function getAdPlaceDetail($request){
		$id = $request->param('id', 0);
		$place = AdPlace::where('id', '=', $id)->findOrEmpty();
		return $place->isEmpty() ? [] : $place;
	}

	/**
	 * @title 创建广告位
	 *
	 * @param [type] $request
	 * @return void
	 */
	public function createAdPlace($request){
		$data = $request->post();

		return AdPlace::create($data);
	}

	public function updateAdPlace($request){
		$data = $request->post();

		return AdPlace::update($data, ['id' => $data['id']]);
	}

	public function getAdByPlace($request){
		$param = $request->param();
		$order = "id desc";

		if(isset($param['id']) && $param['id']){
			$map[] = ['place_id', '=', $param['id']];
		}
		
		$list = Ad::where($map)->order($order)->paginate($request->pageConfig);
		return $list;
	}
}