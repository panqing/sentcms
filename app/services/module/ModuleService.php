<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\services\module;

use app\model\module\Model;
use think\facade\Cache;
use think\facade\Route;

class ModuleService{
	
	/**
	 * @title 获取模型数据
	 */
	public function getModuleList(){
		$map = [];

		$list = (new Module())->where($map)->select();

		return $list;
	}

	public function registerRoute(){
		$model = Cache::get('model_route_list');
		if (!$model) {
			$model = Model::where('status', '>', 0)->field(['id', 'name'])->select()->toArray();
			Cache::set('model_route_list', $model);
		}

		if (!empty($model)) {
			foreach ($model as $value) {
				Route::rule('/admin/' . $value['name'] . '/:function', 'admin.Content/:function')->append(['name'=>$value['name'], 'model_id' => $value['id']]);
				Route::rule($value['name'] . '/index', 'front.Content/index')->append(['name'=>$value['name'], 'model_id' => $value['id']]);
				Route::rule($value['name'] . '/list/:id', 'front.Content/lists')->append(['name'=>$value['name'], 'model_id' => $value['id']]);
				Route::rule($value['name'] . '/detail-:id', 'front.Content/detail')->append(['name'=>$value['name'], 'model_id' => $value['id']]);
				Route::rule('/user/' . $value['name'] . '/:function', 'user.Content/:function')->append(['name'=>$value['name'], 'model_id' => $value['id']]);
				Route::rule('/api/' . $value['name'] . '/:function', 'api.Content/:function')->append(['name'=>$value['name'], 'model_id' => $value['id']]);
			}
		}
	}
}