<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\services\system;

use app\model\system\Rewrite;
use think\facade\Cache;
use think\facade\Route;

class RewriteService{

	public function registerRoute(){
		$rewrite = Cache::get('rewrite_list');
		if (!$rewrite) {
			$rewrite = Rewrite::select()->toArray();
			Cache::set('rewrite_list', $rewrite);
		}

		if (!empty($rewrite)) {
			foreach ($rewrite as $key => $value) {
				$url = parse_url($value['url']);
				$param = [];
				parse_str($url['query'], $param);
				Route::rule($value['rule'], $url['path'])->append($param);
			}
		}
	}
}