<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\services\system;

use app\model\system\Config;
use think\facade\Cache;

class ConfigService{

	/**
	 * @title 获取配置列表
	 *
	 * @param [type] $request
	 * @return void
	 */
	public function getConfigList($request){
		$group = $request->param('group', 0);
		$name = $request->param('name', 0);
		
		/* 查询条件初始化 */
		$map = [];
		if ($group) {
			$map['group'] = $group;
		}

		if ($name) {
			$map['name'] = array('like', '%' . $name . '%');
		}

		$list = Config::where($map)->order('id desc')->paginate($request->pageConfig);
		return $list;
	}

	/**
	 * @title 根据分组获取配置
	 *
	 * @param [type] $request
	 * @return void
	 */
	public function getConfigByGroup($request){
		$id = $request->param('id', 1);
		$map = [];
		
		$map[] = ['group', '=', $id];
		$map[] = ['status', '=', 1];
		$data = Config::where($map)->field('id,name,title,extra,value,remark,type')->order('sort')->select();

		return $data ? $data : [];
	}

	/**
	 * @title 更新配置
	 *
	 * @param [type] $request
	 * @return void
	 */
	public function updateConfig($request){
		$data = $request->post('config');

		foreach ($data as $key => $value) {
			Config::update(['value' => $value], ['name' => $key]);
		}
		//清除db_config_data缓存
		Cache::delete('system_config_data');
	}

	/**
	 * @title 创建配置项
	 *
	 * @param [type] $request
	 * @return void
	 */
	public function createConfig($request){
		$data = $request->post();
		$result = Config::create($data);
		if (false !== $result) {
			Cache::pull('system_config_data');
		} else {
			throw new \think\Exception("新增失败！", 1);
		}
	}

	/**
	 * @title 获取单个配置信息
	 *
	 * @return void
	 */
	public function getConfigDetail($request){
		$id = $request->param('id', 0);

		$config = Config::where('id', '=', $id)->findOrEmpty();
		return $config->isEmpty() ? [] : $config;
	}

	/**
	 * @title 修改单个配置项
	 *
	 * @param [type] $request
	 * @return void
	 */
	public function editConfig($request){
		$data = $request->post();

		$result = Config::update($data, array('id' => $data['id']));
		if (false !== $result) {
			Cache::pull('system_config_data');
			return true;
		} else {
			throw new \think\Exception("更新失败！", 1);
		}
	}

	/**
	 * @title 删除配置
	 *
	 * @return void
	 */
	public function deleteConfig($request){
		$id = $request->param('id', 0);
		if(!$id){
			throw new \think\Exception("非法操作！", 1);
		}
		return Config::find($id)->delete();
	}

	public function getThemesList($request){
		return [
			'pc'  => $this->getList('pc'),
			'mobile' => $this->getList('mobile')
		];
	}

	protected function getList($type){
		$tempPath = app()->getRootPath() . 'public' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;

		$file  = opendir($tempPath);
		$list = [];
		while (false !== ($filename = readdir($file))) {
			if (!in_array($filename, array('.', '..'))) {
				$files = $tempPath . $filename . '/info.php';
				if (is_file($files)) {
					$info = include($files);
					if (isset($info['type']) && $info['type'] == $type) {
						$info['id']  = $filename;
						$preview = '/template/' . $filename . '/' . $info['preview'];
						$info['preview'] = is_file($tempPath . $preview) ? $preview : '/static/common/images/default.png';
						$list[] = $info;
					}else{
						continue;
					}
				}
			}
		}
		return $list;
	}
}