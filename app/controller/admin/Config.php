<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\controller\admin;

use app\services\system\ConfigService;
use think\facade\Cache;

/**
 * @title 配置管理
 */
class Config extends Base {

	/**
	 * @title 配置管理
	 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
	 */
	public function index(ConfigService $config) {
		$list = $config->getConfigList($this->request);

		// 记录当前列表页的cookie
		Cookie('__forward__', $_SERVER['REQUEST_URI']);

		$this->data = array(
			'group' => config('config_group_list'),
			'config_type' => config('config_config_list'),
			'page' => $list->render(),
			'group_id' => $this->request->param('group', 0),
			'list' => $list,
		);

		return $this->fetch();
	}

	/**
	 * @title 信息配置
	 */
	public function group(ConfigService $config) {
		if ($this->request->isPost()) {
			try {
				$config->updateConfig($this->request);
				return $this->success("更新成功！");
			} catch (\think\Exception $e) {
				return $this->error($e->getMessage());
			}
		} else {
			$this->data = [
				'list' => $config->getConfigByGroup($this->request),
				'id' => $this->request->param('id', 1)
			];
			return $this->fetch();
		}
	}

	/**
	 * @title 新增配置
	 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
	 */
	public function add(ConfigService $config) {
		if ($this->request->isPost()) {
			try {
				$config->createConfig($this->request);

				return $this->success('添加成功！', url('/admin/config/index'));
			} catch (\think\Exception $e) {
				return $this->error($e->getMessage());
			}
		} else {
			$this->data['info'] = [];
			return $this->fetch('edit');
		}
	}

	/**
	 * @title 编辑配置
	 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
	 */
	public function edit(ConfigService $config) {
		if ($this->request->isPost()) {
			try {
				$config->editConfig($this->request);
				return $this->success("修改成功！", url('/admin/config/index'));
			} catch (\think\Exception $e) {
				return $this->error($e->getMessage());
			}
		} else {
			$this->data = ['info' => $config->getConfigDetail($this->request)];
			return $this->fetch();
		}
	}

	/**
	 * @title 删除配置
	 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
	 */
	public function del(ConfigService $config) {
		try {
			$config->deleteConfig($this->request);
			Cache::pull('system_config_data');
			return $this->success('删除成功');
		} catch (\think\Exception $e) {
			return $this->error($e->getMessage());
		}
	}

	/**
	 * @title 主题选择
	 */
	public function themes(ConfigService $config) {
		$list = $config->getThemesList($this->request);
		$config = Cache::get('system_config_data');
		
		$this->data = array(
			'pc' => isset($config['pc_themes']) ? $config['pc_themes'] : '',
			'mobile' => isset($config['mobile_themes']) ? $config['mobile_themes'] : '',
			'list' => $list,
		);
		return $this->fetch();
	}

	/**
	 * @title 设置主题
	 * @return json
	 */
	public function setthemes($name, $id) {
		$result = ConfigM::update(['value' => $id], ['name' => $name."_themes"]);
		if (false !== $result) {
			Cache::pull('system_config_data');
			return $this->success('设置成功！');
		} else {
			return $this->error('设置失败！');
		}
	}
}